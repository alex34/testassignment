#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <vector>
#include <utility>

using namespace std;

// first represent time in minutes since 00:00
// second = true if it's enter or car, false if leave
typedef pair<int,bool> entry;

inline int fast_convert (const char *line)
{
    int value = 0;
    value += (line[0] - '0') * 10 * 60;
    value += (line[1] - '0') * 60;
    value += (line[3] - '0') * 10;
    value += (line[4] - '0');
    return value;
}

inline string to_time (int i)
{
    string s;
    s +=  (i / 600)  + '0'; i -= (i / 600) * 600;
    s += i / 60 + '0'; i -= (i / 60) * 60;
    s += ':';
    s += i / 10 + '0'; i -= (i / 10) * 10;
    s += i + '0';

    return s;
}

// output param is a vector of entries
// returns true if parsed ok and all data are correct.
bool ParseFile (ifstream& file, vector<entry>& result)
{
    regex lineTemplate("[0-2][0-9]:[0-5][0-9] [0-2][0-9]:[0-5][0-9]");
    string s;
    vector<entry> entryChart;

    while(getline (file, s))
    {
        if (regex_match(s, lineTemplate)) {
            int entTime = fast_convert(s.substr(0, 5).c_str());
            int extTime = fast_convert(s.substr(6, 5).c_str());
            if (entTime > 1439 || extTime > 1439 || entTime > extTime)
                return false;
            entryChart.push_back (entry(entTime, true));
            entryChart.push_back (entry(extTime, false));
        }
        else
            return false;
    }
    result = entryChart;
    return true;
}

int main(int argc, char* argv[])
{
    if (argc != 2) {
        if (argc > 2) cout << "too many arguments" << endl;
        cout << "usage: " << argv[0] << " <input file>" << endl;
        return -1;
    }

    ifstream inputFile;
    inputFile.open (argv[1]);
    if (!inputFile.is_open())
    {

        cout << "Can't open file" << endl;
        return -1;
    }

    vector<entry> CarsTimesheet;
    if (!ParseFile(inputFile, CarsTimesheet))
    {
        cout << "Error parsing file or wrong data" << endl;
        inputFile.close();
        return -1;
    }
    inputFile.close();

    // two assumptions:
    // 1) if one car enters and another leaves at the same minute,
    // they are considere overlapping
    // 2) if several cars enter/leave at the same minute, it is considered
    // that all entered cars are overlapped
    // To acheive #2 - we put all enters before all leaves if times are equal
    sort (CarsTimesheet.begin(), CarsTimesheet.end(), [](entry a, entry b) { if (a.first == b.first)
                                                                                return a.second;
                                                                             return a.first < b.first; });

    int numberOfCars = 0;
    int maxNumberOfCars = 0;
    int EnterTime = 0;
    vector<pair<int,int>> maxPeriods;

    for (auto entry :CarsTimesheet) {
        if (entry.second) numberOfCars++;
            else numberOfCars--;

            // found new busiest period
        if (numberOfCars > maxNumberOfCars) {
            maxPeriods.clear();
            EnterTime = entry.first;
            maxNumberOfCars = numberOfCars;
        }
            // leaving busiest period
        if ((numberOfCars == maxNumberOfCars - 1) && !entry.second)
                    maxPeriods.push_back(pair<int,int>(EnterTime, entry.first));
            // found one more busiest period
        if ((numberOfCars == maxNumberOfCars) && entry.second)
            EnterTime = entry.first;
    }

    cout << "Busiest periods:" << endl;
    for (auto s: maxPeriods)
        cout << to_time(s.first) << "-" << to_time(s.second) << endl;
    cout << "with " << maxNumberOfCars << " cars on the parking" << endl;

    return 0;
}


