#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <vector>
#include <utility>

using namespace std;

typedef pair<int,int> entry;

inline int fast_convert (const char *line)
{
    int value = 0;
    value += (line[0] - '0') * 10 * 60;
    value += (line[1] - '0') * 60;
    value += (line[3] - '0') * 10;
    value += (line[4] - '0');
    return value;
}

inline string to_time (int i)
{
    string s;
    s +=  (i / 600)  + '0'; i -= (i / 600) * 600;
    s += i / 60 + '0'; i -= (i / 60) * 60;
    s += ':';
    s += i / 10 + '0'; i -= (i / 10) * 10;
    s += i + '0';

    return s;
}

// output param is a vector of pairs, representing
// enter and leave times in minutes sinse beginning of the day
// returns true if parsed ok and all data are correct.
bool ParseFile (ifstream& file, vector<entry>& result)
{
    regex lineTemplate("[0-2][0-9]:[0-5][0-9] [0-2][0-9]:[0-5][0-9]");
    string s;
    vector<entry> entryChart;

    while(getline (file, s))
    {
        if (regex_match(s, lineTemplate)) {
            int entTime = fast_convert(s.substr(0, 5).c_str());
            int extTime = fast_convert(s.substr(6, 5).c_str());
            if (entTime > 1439 || extTime > 1439 || entTime > extTime)
                return false;
            entryChart.push_back (entry(entTime, extTime));
        }
        else
            return false;
    }
    result = entryChart;
    return true;
}

// calculates overlaps of time periods, starting with "start" position
// input periods in inputChart vector, resulted overlap periods in outputChart,
// order represent amount of cars being at the same time on the parking
// with the original one.
bool CalculateOverlaps (const int start, vector<entry>& inputChart, vector<entry>& outputChart, int& order)
{
    int _order = 0;

    vector <entry> overlaps;
    for (uint i = start + 1; i < inputChart.size(); i++ ) {
        if (inputChart.at(i).first > inputChart.at(start).second) break; // no overlaps anymore
        int startTime = inputChart.at(i).first;
        int endTime = inputChart.at(start).second < inputChart.at(i).second ?
                                                inputChart.at(start).second :
                                                    inputChart.at(i).second;
        overlaps.push_back(entry(startTime, endTime));
        _order = 1;
    }
    if (overlaps.size() == 0) { order = 0; outputChart = inputChart; return false; }
    // recursive call to search for higher order overlaps
    int j;
    CalculateOverlaps(0, overlaps, outputChart, j);
    _order += j;

    order = _order;
    return true;
}

int main(int argc, char* argv[])
{
    if (argc != 2) {
        if (argc > 2) cout << "too many arguments" << endl;
        cout << "usage: " << argv[0] << " <input file>" << endl;
        return -1;
    }

    ifstream inputFile;
    inputFile.open (argv[1]);
    if (!inputFile.is_open())
    {

        cout << "Can't open file" << endl;
        return -1;
    }

    vector<entry> CarsTimesheet;
    if (!ParseFile(inputFile, CarsTimesheet))
    {
        cout << "Error parsing file or wrong data" << endl;
        inputFile.close();
        return -1;
    }
    inputFile.close();

    // sort by accending of arriving times
    std::sort (CarsTimesheet.begin(), CarsTimesheet.end(), [](const entry a, const entry b) { return a.first < b.first;});

    int maxCarsNumber = 0;      // maximum overlaps
    vector<entry> maxOverlaps;

    int currentCarsNumber = 0;
    vector<entry> currentOverlaps;

    for (uint i = 0; i < CarsTimesheet.size(); i++) {
        CalculateOverlaps (i, CarsTimesheet, currentOverlaps, currentCarsNumber);
        // total cars number - overlaps with origin car
        // plus the car itself.
        currentCarsNumber++;
        if (currentCarsNumber > maxCarsNumber) { maxCarsNumber = currentCarsNumber; maxOverlaps = currentOverlaps; }
        else if (currentCarsNumber == maxCarsNumber) {  // we should consider all periods with max number of cars
                maxOverlaps.insert(maxOverlaps.end(), currentOverlaps.begin(), currentOverlaps.end());
            }
    }

    cout << "Busiest periods:" << endl;
    for (auto s: maxOverlaps)
        cout << to_time(s.first) << "-" << to_time(s.second) << endl;
    cout << "with " << maxCarsNumber << " cars on the parking" << endl;

    return 0;
}


