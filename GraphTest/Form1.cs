﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

using entry = System.Tuple<int, bool>;

namespace GraphTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            // only hours and mins will be used.
            chart1.ChartAreas[0].AxisX.LabelStyle.Format = "HH:mm";
            chart1.ChartAreas[0].AxisX.Minimum = new DateTime(2000, 1, 1, 00, 00, 00).ToOADate(); ;
            chart1.ChartAreas[0].AxisX.Maximum = new DateTime(2000, 1, 1, 23, 59, 00).ToOADate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Stream filestream;
            openFileDialog1.InitialDirectory = Directory.GetCurrentDirectory();
            if (openFileDialog1.ShowDialog() == DialogResult.OK && (filestream = openFileDialog1.OpenFile()) != null)
                BuildChart(filestream);
        }

        internal int fast_convert(byte[] line)
        {
            int value = 0;
            value += (line[0] - '0') * 10 * 60;
            value += (line[1] - '0') * 60;
            value += (line[3] - '0') * 10;
            value += (line[4] - '0');
            return value;
        }

        // output param is a vector of entries
        // returns true if parsed ok and all data are correct.
        bool ParseFile(Stream file, List<entry> result)
        {
            Regex regex = new Regex("[0-2][0-9]:[0-5][0-9] [0-2][0-9]:[0-5][0-9]");

            string s;
            using (StreamReader streamReader = new StreamReader(file))
            {
                while ((s = streamReader.ReadLine()) != null)
                {
                    if (regex.IsMatch(s))
                    {
                        int entTime = fast_convert(Encoding.ASCII.GetBytes(s.Substring(0, 5)));
                        int extTime = fast_convert(Encoding.ASCII.GetBytes(s.Substring(6, 5)));
                        if (entTime > 1439 || extTime > 1439 || entTime > extTime)
                            return false;
                        result.Add(new entry(entTime, true));
                        result.Add(new entry(extTime, false));
                    }
                    else
                        return false;
                }
            }
            return true;
        }

        int Comparer(entry a, entry b)
        {
            if (a.Item1 == b.Item1)
            {
                if (a.Item2 == false && b.Item2 == true) return -1;
                if (a.Item2 == true && b.Item2 == true) return 0;
                if (a.Item2 == true && b.Item2 == false) return 1;
            }
            else
            {
                if (a.Item1 > b.Item1) return 1;
                if (a.Item1 == b.Item1) return 0;
                if (a.Item1 < b.Item1) return -1;
            }
            return 0; // default, will never get there
        }

        private void BuildChart(Stream file)
        {
            List<entry> CarsTimesheet = new List<entry>();
            if (!ParseFile(file, CarsTimesheet))
            {
                MessageBox.Show("Can't parse file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            file.Close();

            // two assumptions:
            // 1) if one car enters and another leaves at the same minute,
            // they are considere overlapping
            // 2) if several cars enter/leave at the same minute, it is considered
            // that all entered cars are overlapped
            // To acheive #2 - we put all enters before all leaves if times are equal
            CarsTimesheet.Sort(Comparer);

            int numberOfCars = 0;
            Series newPlot = new Series
            {
                ChartType = SeriesChartType.Area,
                XValueType = ChartValueType.DateTime,
                IsVisibleInLegend = false
            };
            int prev = 0;
            DateTime currentTime;
            
            foreach (var ent in CarsTimesheet)
            {
                if (ent.Item2) numberOfCars++;
                    else numberOfCars--;
                currentTime = new DateTime(2000, 1, 1, ent.Item1 / 60, ent.Item1 - (ent.Item1 / 60) * 60, 0);
                newPlot.Points.AddXY(currentTime, prev);
                newPlot.Points.AddXY(currentTime, numberOfCars);
                prev = numberOfCars;
            }

            chart1.Series.Clear();
            chart1.Series.Add(newPlot);
        }
    }
}
