// JsonTestAssignment.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "include/rapidjson/document.h"
#include "include/rapidjson/filereadstream.h"
#include <iostream>
#include <vector>
#include <ctime>
#include <algorithm>

using namespace rapidjson;
using namespace std;

// first represent time in minutes since epoch
// second = true if it's arrival of the car, false if it's leave
typedef pair<time_t, bool> entry;

// size of temp buffer for parsing json
const int BUFFERSIZE = 65536;

int main(int argc, char* argv[])
{
	if (argc != 2) {
		if (argc > 2) cout << "too many arguments" << endl;
		cout << "usage: " << argv[0] << " <input file>" << endl;
		return -1;
	}

	FILE* file;
	if (fopen_s(&file, argv[1], "rb")) {
		cout << "Can't open file" << endl;
		return -1;
	}
	
	Document doc;
	char buffer[BUFFERSIZE];
	FileReadStream is(file, buffer, BUFFERSIZE);
	ParseResult result = doc.ParseStream(is);
	fclose(file);
	
	if (!result || !doc.IsArray()) {
		cout << "Error of parsing file" << endl;
		return -1;
	}

	vector<entry> CarsTimesheet;
	bool chartCreated = true;
	tm t; 
	time_t arrivalTime = 0, leaveTime = 0;

	for (auto& s : doc.GetArray()) {
		if (s.HasMember("ArrivalTime") && s["ArrivalTime"].IsString()) {
			int j = sscanf_s( s["ArrivalTime"].GetString(), 
				              "%d-%d-%dT%d:%d:%d", 
				              &t.tm_year, &t.tm_mon, &t.tm_mday, &t.tm_hour, &t.tm_min, &t.tm_sec);
			if (j < 6) { chartCreated = false; break; }
			t.tm_year -= 1900; 
			t.tm_mon--;
			arrivalTime = mktime(&t);
		}
		else { chartCreated = false; break;	}

		if (s.HasMember("LeaveTime") && s["LeaveTime"].IsString()) {
			int j = sscanf_s( s["LeaveTime"].GetString(), 
				              "%d-%d-%dT%d:%d:%d", 
				              &t.tm_year, &t.tm_mon, &t.tm_mday, &t.tm_hour, &t.tm_min, &t.tm_sec);
			if (j < 6) { chartCreated = false; break; }
			t.tm_year -= 1900; t.tm_mon--;
			leaveTime = mktime(&t);
		}
		else { chartCreated = false; break;	}
		if (leaveTime < arrivalTime) { chartCreated = false; break; }
		CarsTimesheet.push_back(entry(arrivalTime, true));
		CarsTimesheet.push_back(entry(leaveTime, false));
	}
	
	if (!chartCreated) {
		cout << "Can't create time chart" << endl;
		return -1;
	}

	// two assumptions:
	// 1) if one car enters and another leaves at the same minute,
	// they are considered to be overlapping
	// 2) if several cars enter/leave at the same minute, it is considered
	// that all entered cars are overlapped
	// To acheive #2 - we put all enters before all leaves if times are equal
	sort(CarsTimesheet.begin(), CarsTimesheet.end(), [](entry a, entry b) { if (a.first == b.first)
	 	                                                                       return (a.second == true && b.second == false);
	                                                                        return a.first < b.first; });

	int numberOfCars = 0;
	int maxNumberOfCars = 0;
	vector<pair<time_t, time_t>> maxPeriods;

	for (auto entry : CarsTimesheet) {
		if (entry.second) numberOfCars++;
		else numberOfCars--;

		// found new busiest period
		if (numberOfCars > maxNumberOfCars) {
			maxPeriods.clear();
			arrivalTime = entry.first;
			maxNumberOfCars = numberOfCars;
		}
		// leaving busiest period
		if ((numberOfCars == maxNumberOfCars - 1) && !entry.second)
			maxPeriods.push_back(pair<time_t, time_t>(arrivalTime, entry.first));
		// found one more busiest period
		if ((numberOfCars == maxNumberOfCars) && entry.second)
			arrivalTime = entry.first;
	}
	
	char arrivalTimeLine[32];
	char leaveTimeLine[32];
	tm arrivalTM, leaveTM;

	cout << "Busiest periods:" << endl;
	for (auto s : maxPeriods) {
		localtime_s(&arrivalTM, &s.first);
		localtime_s(&leaveTM, &s.second);
		strftime(arrivalTimeLine, 32, "%D %R", &arrivalTM);
		strftime(leaveTimeLine, 32, "%D %R", &leaveTM);
		cout << arrivalTimeLine << " - " << leaveTimeLine << endl;
	}
	cout << "with " << maxNumberOfCars << " cars on the parking" << endl;

	return 0;
}